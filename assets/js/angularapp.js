var r_app=angular.module('angularApp',['ngRoute','ngAnimate','firebase','oc.lazyLoad']);


r_app.factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
        return $firebaseAuth();
    }
]);

r_app.run(['$rootScope', '$location', '$firebaseAuth', '$ocLazyLoad', function($rootScope, $location, $firebaseAuth, $ocLazyLoad){
    $rootScope.auth = $firebaseAuth();
    $rootScope.error = '';
    $rootScope.title = 'Website';
    $rootScope.authUser="Asd";
    // any time auth state changes, add the user data to scope
    $rootScope.auth.$onAuthStateChanged(function(firebaseUser) {
        $rootScope.authUser = firebaseUser;
        if(firebaseUser && ($location.path() == '/register' || $location.path() == '/login')) $location.path('/');
    });
       

    

    $rootScope.$on('$routeChangeStart', function() {
        $rootScope.navfix="g-bg-white ";
        $rootScope.navexc="";
        $rootScope.navfirst="g-bg-white bg-white";

        

        $rootScope.nav_buttons="";

        $('.modal-backdrop').remove();
    })

    $rootScope.$on('$routeChangeError', function(event, next, previous, error) {
        if (error == 'AUTH_REQUIRED') {
            $rootScope.error = 'Sorry, you must be a registered user.'
            $location.path('/login');
        }
        
    });
}]);

r_app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "pages/home.html",
        controller: 'HomeController'
    })
    .when("/login", {
        templateUrl : "pages/login.html",
        controller: 'LoginController',
        resolve: {
            'currentAuth': ['Auth','$location', function(Auth,$location) {
                if(Auth.$getAuth()) 
                    $location.path('/order-history');
            }]
        }
    })
    .when("/register", {
        templateUrl : "pages/signup.html",
        controller: 'LoginController',
        resolve: {
            'currentAuth': ['Auth','$location', function(Auth,$location) {
                if(Auth.$getAuth()) 
                    $location.path('/order-history');
            }]
        }
    })
    .when("/reset/password", {
        templateUrl : "pages/reset.html",
        controller: 'LoginController'
    })
    .when("/account", {
        templateUrl : "pages/account.html",
        controller: 'AccountController',
        // resolve: {
        //     'currentAuth': ['Auth', function(Auth) {
        //         return Auth.$requireSignIn();
        //     }]
        // }
    })
    .when("/order-history", {
        templateUrl : "pages/orderhistory.html",
        controller: 'AccountController',
        resolve: {
            'currentAuth': ['Auth', function(Auth) {
                return Auth.$requireSignIn();
            }]
        }
    })
    .when("/restaurants", {
        templateUrl : "pages/restaurants.html",
        controller: 'RestaurantController'
    })
    .when("/restaurants/:alias", {
        templateUrl : "pages/dashboard.html",
        controller: 'RestaurantController'
    })
    .when("/partnership", {
        templateUrl : "pages/partnership.html",
        controller: 'OthersController'
    })
    .when("/contact", {
        templateUrl : "pages/contact.html",
        controller: 'OthersController'
    })
    .when("/terms-and-conditions", {
        templateUrl : "pages/terms.html",
        controller: 'OthersController'
    })
    .otherwise({
        redirectTo: '/'
    });

    // $locationProvider.html5Mode(true);
});

r_app.controller('LoginController',function($scope, $rootScope, $location){
    
    $scope.login=function(email,password){
        
        $rootScope.error = null;
        $rootScope.auth.$signInWithEmailAndPassword(email, password).then(function(firebaseUser) {
            $rootScope.authUser = firebaseUser.user;
            $location.path('/order-history');

        }).catch(function(error) {
            if(error.code && (error.code == "auth/user-not-found") || error.code == 'auth/wrong-password')
                $rootScope.error = 'Invalid credentials';
            else
                $rootScope.error = 'Something went Wrong. Please try again later.';

        });
    }

    $scope.forgotPassword = function(email) {
        $rootScope.auth.$sendPasswordResetEmail(email).then(function(){
            $rootScope.success = "Check your e-mail to reset your password.";
        }).catch(function(error) {
            $rootScope.error = error.message;
        });
    }
});

r_app.controller('RestaurantController',function($scope, $rootScope, $routeParams, $location, $ocLazyLoad){
        // $ocLazyLoad.load('./common.js')

    $scope.mainApp=0;
    
    $scope.getRestaurants = function(){
        
        // $ocLazyLoad.load('./assets/js/common.js')
        
        $scope.restaurants = [
            {
                name:'Itihas Restaurant Parmathha',
                alias: 'itihas',
                description:'Itihas Restaurant Parmathha'
            },
            {
                name:'New Sky Restaurant',
                alias: 'new-sky',
                description:'New Sky Restaurant'
            },
            {
                name:'Destiny Restaurant',
                alias: 'destiny',
                description:'Destiny Restaurant'
            }
        ];
    }

    $scope.getRestaurant = function(){
        $ocLazyLoad.load('./../assets/js/now-ui-dashboard.js');
        $scope.restaurants = [
            {
                name:'Itihas Restaurant Parmathha',
                alias: 'itihas',
                description:'Itihas Restaurant Parmathha'
            },
            {
                name:'New Sky Restaurant',
                alias: 'new-sky',
                description:'New Sky Restaurant'
            },
            {
                name:'Destiny Restaurant',
                alias: 'destiny',
                description:'Destiny Restaurant'
            }
        ];

        for(var i=0; i<$scope.restaurants.length; i++){
            if($scope.restaurants[i].alias == $routeParams.alias){
                $scope.restaurant = $scope.restaurants[i];
                break;
            }
        }

        if(!$scope.restaurant){
            alert(404);
            $location.path('/restaurants');
        }

        $('.datetimepicker#datetimepicker1').datetimepicker({
            inline: true,
            format: 'DD/MM/YYYY',
            
    
        }).on('dp.show dp.change', function () {
          // console.log('first dp1',$scope.s1,$scope.s2);
    
              
          // console.log('last dp1',$scope.s1,$scope.s2);
    
        }      
        ).on('dp.update',function(){
        
        });
        $('.datetimepicker#datetimepicker2').datetimepicker({
            inline: true,
            format: 'LT',
            
    
        }).on('dp.show dp.change', function () {
            // console.log('first dp1',$scope.s1,$scope.s2);
    
                
            // console.log('last dp1',$scope.s1,$scope.s2);
    
        }).on('dp.update',function(){
        
        });
        $('.datetimepicker#datetimepicker3').datetimepicker({
                inline:true,
                format: 'DD/MM/YYYY'
        }).on('dp.show dp.change', function () {
            // console.log('first dp1',$scope.s1,$scope.s2);
    
                
            // console.log('last dp1',$scope.s1,$scope.s2);
    
        }).on('dp.update',function(){
            
        });
            
    }

    $scope.changePassword = function(oldPassword, newPassword) {
        var user = firebase.auth().currentUser;

        var credentials = firebase.auth.EmailAuthProvider.credential($scope.authUser.email, oldPassword);
        console.log(firebase)
        user.reauthenticateWithCredential(credentials).then(() => {

            auth.$updatePassword(newPassword).then(function() {
                console.log("Password changed successfully!");
            }).catch(function(error) {
                console.error("Error: ", error);
            });
        })
        .catch(error => {
            if(error.code && error.code == 'auth/wrong-password')
                $scope.error = error.message;
            else
                $scope.error = 'Something went Wrong. Please try again later.';

        })
    }

    $scope.logout = function(){
        $rootScope.auth.$signOut().then(function() {
            
        }, function(error) {
            $rootScope.error =  "Something went wrong";
        });		
    }
});

r_app.controller('HomeController',function($scope,$rootScope){
    $rootScope.navexc="g-bg-black-opacity-0_1";
    $rootScope.navfix="g-bg-white";
    $rootScope.navfirst="g-bg-black-opacity-0_1";

    $rootScope.nav_buttons="un_toggle_color";

    const colors = ['rgb(235,21,54)','rgb(235,130,21)','rgb(29,162,20)'];

    const c = colors[Math.floor(Math.random() * 3)];

    $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

    $('.js-carousel-sync-for').on('init', function (event, slick) {
      $(slick.$slides).css('height', 'auto');
    });

    $('.js-carousel-sync-nav').on('init', function (event, slick) {
      $(slick.$slides).css('height', 'auto');
    });
    $scope.color = {
        'background-color': c,
    }
    $scope.right_partner={
        'background': c,
        'height':'20rem',
        // 'clip-path': 'polygon(40% 0, 100% 0, 100% 100%, 0 100%)'
    }
    $scope.left_partner={
        'background': c,
        'height':'20rem',
    }

    $scope.diagonalsplitbackground = {
        'background-color': c,
        'background-image': '-webkit-linear-gradient(140deg, '+c+' 70%, #6868d4 20%)'
    };
});

r_app.controller('AccountController',function($scope){
    const colors = ['rgb(235,21,54)','rgb(235,130,21)','rgb(29,162,20)'];

    var c = colors[Math.floor(Math.random() * 3)];

    $scope.color = {
        'background-color': c,
    };

    $scope.diagonalsplitbackground = {
        'background-color': c,
        'background-image': '-webkit-linear-gradient(140deg, '+c+' 75%, #6868d4 20%)'
    };
});

r_app.controller('OthersController',function($scope){
    const colors = ['rgb(235,21,54)','rgb(235,130,21)','rgb(29,162,20)'];

    $scope.color = {
        'background-color': colors[Math.floor(Math.random() * 3)],

    }
});


